---
title: "Johdanto, ohjelmistot ja versiohallinta"
output:
  html_document:
    toc: true
    toc_float: true
    number_sections: yes
    code_folding: show
---

***
***

# Ohjelmistoympäristön pystyttäminen

1. Kirjaudu Slackiin <https://utur2016.slack.com/>
2. Avaa uusi ikkuna/välilehti ja kirjaudu **haka**-tunnuksilla CSC:n **Pouta Blueprints**-palveluun <https://pb.csc.fi/#/>
3. Valitse oikealta otsikon *HAMK Introduction to Data Science* alta **Launch new** *(Tähän tulee tdn oma linkki tälle kurssille)*

## gitin konffaaminen

Kun olet saanut avattua Rstudion avaa pääte **Tools** -> **Shell** ja kirjoita

- `git config --global user.name "Etunimi Sukunimi"`
- `git config --global user.email "sun_utu@utu.fi"`

Jatketaan vielä päätteessä ja luodaan .ssh avaimet jotta tunnistautuminen gitlab:iin helpompaa.

- `ssh-keygen -t rsa -b 4096 -C "sun_utu@utu.fi"` - ja 
    1. paina entteriä ja hyväksyen oletussijainti
    2. annan salausavaimen salasana (joku jonka muistat!!)

Printtaa julkinen ssh-avain päätteeseen `cat ~/.ssh/id_rsa.pub` ja kopioi se (`Ctrl+c`) leikepöydälle  ja kopioi kaikki teksti joka alkaa `ssh-rsa` ja päättyy sähköpostiosoitteeseesi.

## Gitlabin konffaaminen

Siirry selaimella [gitlabiin](https://gitlab.com/users/sign_in) ja 

1. klikkaa oikealta **profile settings** -> **SSH keys**.
2. Kopioi isompaan ruutuun julkinen ssh-avain (`Ctrl+v`) ja annan otsikoksi (title) vaikka *sessio1* ja klikkaa **Add key**
3. Siirry osoitteesee: <https://gitlab.com/utur2016/session1_intro> ja klikkaa ruudun keskeltä **Fork**
4. Valitse oma profiilisi, klikkaa **Fork** ja odota hetki
5. Kopioi uuden projektin ssh-osoite ruudun keskeltä joka muotoa: `git@gitlab.com:SINUNOMATUNNUS/session1_intro.git` ja siirry takaisin Rstudioon

## Rstudion konffaaminen

1. Klikkaa oikealta **Project: (None)** ja valitse **New Project** -> **Version Control** -> **Git** ja annan kenttään **repository URL** ssh-osoite ja klikkaa **Create Project**
2. Rstudio lataa gitlab-projektista tiedostot Rstudio-projektiin ja voit aloittaa R:n käytön! 


## Harjoitukset tehdään tässä välissä!


Avaa ensin oikealta alhaalta **Files** välilehdeltä `luento.Rmd`-tiedosto ja paina ruudun ylälaidasta **Knit**-kuvaketta.


```{r fao, eval=FALSE}
# luodaan kansio "data"
dir.create("./data")
# ladataan FAOSTAT-tietokannasta 
download.file(url = "http://fenixservices.fao.org/faostat/static/bulkdownloads/Production_Livestock_E_All_Data_(Norm).zip", 
              mode = "wb",
              destfile = "./data/Production_Livestock_E_All_Data_(Norm).zip")
# puretaan zip
unzip(zipfile = "./data/Production_Livestock_E_All_Data_(Norm).zip", exdir = "./data")
# ladataan data R:ään
d <- read.csv("./data/Production_Livestock_E_All_Data_(Norm).csv", stringsAsFactors = FALSE)
# Kuusi ensimmäistä riviä
head(d)
# rivien ja muuttujien määrät
dim(d)

# Suomessa 2010 tuotettujen eläimien määrät
library(dplyr)
library(ggplot2)

# Suomen elukat
d %>% filter(Country == "Finland",
             Year == 2010,
             Unit == "Head") -> dd

dim(dd)

p <- ggplot(data=dd, aes(x=Item, y=Value))
p <- p + geom_bar(stat="identity", position="dodge")
p 

# Pohjoimaiden elukat
d %>% filter(Country %in% c("Finland","Sweden","Norway","Denmark", "Iceland"),
             Year == 2010,
             Unit == "Head") %>% 
  select(Item,Value,Country) -> dd

dim(dd)

p <- ggplot(data=dd, aes(x=Country, y=Value))
p <- p + geom_bar(stat="identity", position="dodge")
p <- p + facet_wrap(~Item, scales = "free_x")
p 




```





## Omien muutosten tallentaminen Gitlabiin session lopussa!

1. Lopuksi palaa vielä projektiisi Gitlab:ssa ja valitse oikealta hammasratas-ikoni ja sen alta **Members**
2. kirjoita **People**-kenttään `muuankarski` ja valitse *Markus Kainu* ja anna hänelle *developer*-oikeudet
3. Tämän jälkeen palaa vielä Rstudioon ja mene **git** välilehdelle ja 
    1. klikkaa tiedostot joiden muutokset haluat saada talteen ja klikkaa **Commit**
    2. kirjoita ruutuun **muutoksia kuvaava viesti sekä syy muutoksille** ja klikkaa **Commit*
    3. sitten klikkaa ensin **Pull**. anna ssh-salasanasi ja klikkaa **Push**
4. Sulje Rstudio tai aloita jo kotitehtävät!

***
***
***


# Ennakkotehtävät


## Lue


## Katso

- [Open research methods in computational social sciences and humanities: introducing R (Kainu 2014)](https://digihist.se/5-metoder-inom-digital-historia/fordjupning-open-research-methods-in-computational-social-sciences-and-humanities-introducing-r/)

# Miten oppia R

R on vapaa, avoimen lähdekoodin ohjelmisto, jonka

- <https://www.rstudio.com/online-learning/>



# Kurssilla käytettävä R-kielen murre eli ns. "Hadleyverse"


![](graphics/wickham_cycle.png)




1. [Import](#import) your data into R
2. [Tidy](#tidy) it
3. Understand your data by iteratively
    a. [visualizing](#visualize)
    b. [tranforming](#transform) and
    c. [modeling](#modelinfer) your data
4. [Infer](#infer) how your understanding applies to other data sets (_including future data, i.e. predictions_)
5. [Communicate](#communicate) your results to an audience, or
6. [Automate](#automate) your analysis for easy reuse
7. [Program](#program) the whole way through, since you do each of these things on a computer


# Kurssin rakenne


## Import / tuo


R:n perusasennuksessa tulee [joukko datoja](https://stat.ethz.ch/R-manual/R-devel/library/datasets/html/00Index.html) mukana, jotka ovat heti käytettävissä. Saat datan kuvauksen kirjoittamalla `?datan_nimi`. Aluksi käytämme dataa `?mtcars`.

```{r kuvaamtcars}
?mtcars
head(mtcars)
```

Samat datat + paljon muuta löytyy Vincent ylläpitämältä [Rdatasets](https://vincentarelbundock.github.io/Rdatasets/datasets.html)-sivustolta, josta voimme ladata esimerkiksi saman `mtcars`-datan komennolla

```{r lataamtcars}
d <- read.csv("https://vincentarelbundock.github.io/Rdatasets/csv/datasets/mtcars.csv",
                   stringsAsFactors = FALSE)
head(d)
```



# Ohjelmistot

Kurssilla hyödynnetään [Tieteen tietotekniikan keskuksen (CSC)](http://csc,fi) *Pouta Blueprints*  laskentaympäristö, joita opiskelijat voivat käyttää haka-verkon kautta pelkkää selainta käyttäen joko mikroluokkien koneilla tai omalla koneella. Kurssin edetessä halukkaat voivat rakentaa myös vastaavan ympäristön omalle koneelleen omatoimista käyttöä varten.


Kurssilla käytettävät ohjelmistot ovat poikkeuksetta [vapaita](https://fi.wikipedia.org/wiki/Vapaa_ohjelmisto) ja [avoimen lähdekoodin ohjelmistoja](https://fi.wikipedia.org/wiki/Avoin_l%C3%A4hdekoodi), joita voi käyttää sekä Windowsissa, OSX:ssä että eri linux-jakeluissa.

Ohjelmistoympäristö on perusteltu ja käyty läpi hyvin täällä: [Jenny Bryan and the STAT 545 TAs: Happy Git and GitHub for the useR](http://happygitwithr.com/)

## R



### R Studio

- RStudio Full Tour <https://vimeo.com/97378167>

### Rmarkdown

- What is R Markdown? <https://vimeo.com/177254549>
- Communicate-better-with-R-Markdown  <https://vimeo.com/176881928>
- Getting started with R Markdown  <https://vimeo.com/142172484>

### git

- git <https://git-scm.com/>
- The Basics of Git and GitHub <https://www.youtube.com/watch?v=U8GBXvdmHT4>
- Lear Git in 20 minutes <https://www.youtube.com/watch?v=Y9XZQO1n_7c>
- RStudio & git/github Demonstration <https://vimeo.com/119403806>


### shell

- Shell script <https://en.wikipedia.org/wiki/Shell_script>

### Github-koodinjakopalvelu

[Github](https://github.com) ei ole ohjelmisto, vaan kenties suosituin lähdekoodin jakamisen ja yhdessä ohjelmoimisen (social coding) mahdollistava verkkopalvelu. Kurssille osallistuminen edellyttää Github:iin rekisteröitymistä.

## Ympäristöt

### Pilvessä

Kurssin ensisijainen laskentaympäristö on [Tieteen tietotekniikan keskuksen (CSC)](http://csc,fi) *Pouta Blueprints* palvelimilla, joita opiskelijat voivat käyttää haka-verkon kautta pelkkää selainta käyttöen joko mikroluokkien koneilla tai omalla koneella.


## Paikalliset asennukset omille koneille

Kurssin edetessä




# Versiohallinta




# Kotitehtävä
